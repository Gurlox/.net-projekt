﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Script.Serialization;

namespace projekt_net
{
    /// <summary>
    /// Interaction logic for PairView.xaml
    /// </summary>
    public partial class PairView : UserControl
    {
        private List<Person> pair = new List<Person>();

        public PairView(Person firstPerson, Person secondPerson)
        {
            InitializeComponent();
            FName.Text = firstPerson.Name;
            FAge.Text = firstPerson.Age.ToString();

            SName.Text = secondPerson.Name;
            SAge.Text = secondPerson.Age.ToString();

            pair.Add(firstPerson);
            pair.Add(secondPerson);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var request = (HttpWebRequest)WebRequest.Create("https://love-calculator.p.rapidapi.com/getPercentage?fname=" + pair[0].Name + "&sname=" + pair[1].Name);

            request.Headers["X-RapidAPI-Host"] = "love-calculator.p.rapidapi.com";
            request.Headers["X-RapidAPI-Key"] = "8923640504msh6b7ec4786d6cf1fp1ce0a7jsn4e0aace5e627";
            request.Method = "GET";

            var content = string.Empty;

            try
            {
                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    using (var stream = response.GetResponseStream())
                    {
                        using (var sr = new StreamReader(stream))
                        {
                            var responseBody = sr.ReadToEnd();
                            var deserializer = new JavaScriptSerializer();
                            var results = deserializer.Deserialize<LoveResult>(responseBody);
                            MessageBox.Show("Ta para pasuje do siebie w " + results.percentage + " procentach\n" + results.result);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show("Błąd połączenia. Spróbuj ponownie.");
            }
        }
    }
}
