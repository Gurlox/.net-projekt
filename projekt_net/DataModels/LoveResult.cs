﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projekt_net
{
    public class LoveResult
    {
        public string fname { get; set; }
        public string sname { get; set; }
        public string percentage { get; set; }
        public string result { get; set; }
    }
}
