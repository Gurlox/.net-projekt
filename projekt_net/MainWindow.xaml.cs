﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Web.Script.Serialization;

namespace projekt_net
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public string FName { get; set; }
        public int FAge { get; set; }
        public string SName { get; set; }
        public int SAge { get; set; }
        public string ImagePath { get; set; }

        private List<List<Person>> pairs = new List<List<Person>>();

        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
            ImagePath = "J:/dotniet/projekt_net/twarze.jpg";
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Person firstPerson = new Person();
            firstPerson.Name = FName;
            firstPerson.Age = FAge;

            Person secondPerson = new Person();
            secondPerson.Name = SName;
            secondPerson.Age = SAge;

            List<Person> pair = new List<Person>();
            pair.Add(firstPerson);
            pair.Add(secondPerson);
            pairs.Add(pair);

            PairView pairView = new PairView(firstPerson, secondPerson);

            PeopleBox.Items.Add(pairView);
        }

        private async void Button_Click_1(object sender, RoutedEventArgs e)
        {
            string serialized = new JavaScriptSerializer().Serialize(pairs);

            MessageBox.Show(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments));
            try
            {
                System.IO.File.WriteAllText(@"J:\content.txt", serialized);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private async void Button_Click_2(object sender, RoutedEventArgs e)
        {
            try
            {
                using (var reader = File.OpenText(@"J:\content.txt"))
                {
                    string fileText = await reader.ReadToEndAsync();
                    var deserializedResult = new JavaScriptSerializer().Deserialize<List<List<Person>>>(fileText);
                    PeopleBox.Items.Clear();

                    foreach (List<Person> pair in deserializedResult)
                    {
                        PairView pairView = new PairView(pair[0], pair[1]);
                        PeopleBox.Items.Add(pairView);
                    }
                }
            }
            catch (IOException exception)
            {
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(exception.Message);
            }
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            dlg.DefaultExt = ".png";
            dlg.Filter = "JPEG Files (*.jpeg)|*.jpeg|PNG Files (*.png)|*.png|JPG Files (*.jpg)|*.jpg|GIF Files (*.gif)|*.gif";

            Nullable<bool> result = dlg.ShowDialog();
        
            if (result == true)
            {
                // Open document 
                string filename = dlg.FileName;
                Avatar.Source = new BitmapImage(new Uri(filename));
            }
        }
    }
}
